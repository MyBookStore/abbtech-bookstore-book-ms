package org.example.abbtechbookstorebookms.service;

import org.example.abbtechbookstorebookms.dto.BookDto;
import org.example.abbtechbookstorebookms.model.Book;
import org.example.abbtechbookstorebookms.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class BookServiceImpl implements BookService{

    private final BookRepository bookRepository;
    private UUID id;

    public BookServiceImpl(BookRepository bookRepository) {

        this.bookRepository = bookRepository;
    }
    @Override
    public ResponseEntity<String> addBook(BookDto bookDto) {
        Book book=Book.builder()
                .title(bookDto.getTitle())
                .author(bookDto.getAuthor())
                .price(bookDto.getPrice())
                .build();
        bookRepository.save(book);
        return ResponseEntity.ok(book.getTitle()+" Book added");
    }
    @Override
    public ResponseEntity<List<Book>> allBooks() {
        List<Book> books = bookRepository.findAll();
        return ResponseEntity.ok(books);
    }
    @Override
    public Book getById(UUID id) {
        return bookRepository.findById(id).orElse(null);
    }

}
