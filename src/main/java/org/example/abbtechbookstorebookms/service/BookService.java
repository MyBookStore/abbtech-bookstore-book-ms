package org.example.abbtechbookstorebookms.service;

import jakarta.validation.Valid;
import org.example.abbtechbookstorebookms.dto.BookDto;
import org.example.abbtechbookstorebookms.model.Book;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.UUID;

public interface BookService {
    ResponseEntity<String> addBook( @Valid BookDto book);
    ResponseEntity<List<Book>> allBooks();
    public Book getById( @Valid UUID id);
}
