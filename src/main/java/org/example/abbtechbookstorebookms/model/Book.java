package org.example.abbtechbookstorebookms.model;

import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.util.UUID;

@Entity
@Table(name = "Book",schema="public")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Book{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", columnDefinition = "UUID")
    private UUID id;
    @Column(name = "title", length=100)
    private String title;
    @Column(name = "author", length=100)
    private String author;
    @Column(name = "price",precision = 19, scale = 2)
    private BigDecimal price;
}
