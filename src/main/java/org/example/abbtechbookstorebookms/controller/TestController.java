package org.example.abbtechbookstorebookms.controller;

import jakarta.validation.Valid;
import org.example.abbtechbookstorebookms.dto.BookDto;
import org.example.abbtechbookstorebookms.model.Book;
import org.example.abbtechbookstorebookms.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("")
@Validated
public class TestController {
    private final BookService bookService;
    public TestController(BookService bookService) {
        this.bookService = bookService;
    }
@PostMapping("add")
    public ResponseEntity<String> addBook(@RequestBody @Valid BookDto bookDto){
        return bookService.addBook(bookDto);
    }
    @GetMapping("all")
    public ResponseEntity<List<Book>> all(){
        return bookService.allBooks();
    }
    @GetMapping("")
    public ResponseEntity<String> getBook(@RequestParam(value = "bookId", required = false) UUID bookId){
        Book book = bookService.getById(bookId);
        if (book == null) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Book not found");
        }
        System.out.println("Book " + book.getTitle() +" exists");
        return ResponseEntity.ok(book.toString());
    }
}

