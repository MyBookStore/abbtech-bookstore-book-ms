package org.example.abbtechbookstorebookms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AbbtechBookstoreBookMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(AbbtechBookstoreBookMsApplication.class, args);
    }

}
