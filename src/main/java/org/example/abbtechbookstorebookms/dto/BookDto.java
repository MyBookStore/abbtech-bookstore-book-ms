package org.example.abbtechbookstorebookms.dto;

import lombok.Builder;
import lombok.Data;
import java.math.BigDecimal;
import java.util.UUID;

@Data
@Builder
public class BookDto {
    private UUID id;
    private String title;
    private String author;
    private BigDecimal price;
}
