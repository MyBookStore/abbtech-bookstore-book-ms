package org.example.abbtechbookstorebookms.repository;

import org.example.abbtechbookstorebookms.dto.BookDto;
import org.example.abbtechbookstorebookms.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BookRepository extends JpaRepository<Book, UUID> {

}